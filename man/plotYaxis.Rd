\name{plotYaxis}
\alias{plotYaxis}
\title{
  plot y-axis
}
\description{
  plot y-axis for the sequence logo
}
\usage{
plotYaxis(ymax)
}
\arguments{
	\item{ymax}{max value of y axix}
}
\value{
	none
}