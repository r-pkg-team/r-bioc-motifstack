Source: r-bioc-motifstack
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-r,
               r-base-dev,
               r-cran-grimport2,
               r-bioc-motiv,
               r-cran-ade4,
               r-bioc-biostrings,
               r-cran-xml,
               r-cran-scales,
               r-cran-htmlwidgets,
               r-cran-ggplot2
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-motifstack
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-motifstack.git
Homepage: https://bioconductor.org/packages/motifStack/
Rules-Requires-Root: no

Package: r-bioc-motifstack
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends},
         node-d3
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R plot stacked logos for DNA, RNA and amino acid sequence
 The motifStack package is designed for graphic representation of multiple
 motifs with different similarity scores. It works with both DNA/RNA sequence
 motif and amino acid sequence motif. In addition, it provides the flexibility
 for users to customize the graphic parameters such as the font type and
 symbol colors.
